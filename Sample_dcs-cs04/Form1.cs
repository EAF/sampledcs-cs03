﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;


//using Microsoft.Win32;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.IO;
using System.Collections;


namespace Sample_dcs_cs03
{
    //create delegate that allows other threads to send the Form
    delegate void delegateString(char T, string str);
    delegate void delegateString2(string str);
    public partial class Form1 : Form
    {
        //
        Socket clientSocket;
        Socket serverSocket;

        //reading thread
        Thread listenClient;
        Thread listenServer;

        //XML Message ID
        int messageID;

        // XSD informations
        String InterfaceLevel;
        String XSDLocalPath;
        String XSDVersion;

        //read data
        string rtfContent = String.Empty;


        // Vars
        /**/
        String AirlineCode = "TK";

        String ApplicationCode = "EAS2012CUPPT0007";
        String ApplicationName = "KQABC";
        String ApplicationVersion = "01.03";
        String ApplicationData = "CSharp Sample for Cupp-T platform";


        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //resize ListViewEvents Columns
            listViewEvents.Columns[0].Width = 150;
            listViewEvents.Columns[1].Width = 500;

            //select new tabPage
            //tabControlLessons.SelectedTab = tabControlLessons.TabPages[1];

            //load Environment Variables
            loadEnvironmentVariables();

            //load text goals
            loadGoals();

        }
        //load Environment Variables
        private void loadEnvironmentVariables()
        {
            textBoxHost.Text = System.Environment.GetEnvironmentVariable("CUPPSPN", EnvironmentVariableTarget.User);
            textBoxPort.Text = System.Environment.GetEnvironmentVariable("CUPPSPP", EnvironmentVariableTarget.User);

            textBoxPLT.Text = System.Environment.GetEnvironmentVariable("CUPPSPLT", EnvironmentVariableTarget.User);
            textBoxCN.Text = System.Environment.GetEnvironmentVariable("CUPPSCN", EnvironmentVariableTarget.User);
            textBoxACN.Text = System.Environment.GetEnvironmentVariable("CUPPSACN", EnvironmentVariableTarget.User);

            textBoxOPR.Text = System.Environment.GetEnvironmentVariable("CUPPSOPR", EnvironmentVariableTarget.User);
            textBoxUN.Text = System.Environment.GetEnvironmentVariable("CUPPSUN", EnvironmentVariableTarget.User);

            textBoxXSDU.Text = System.Environment.GetEnvironmentVariable("CUPPSXSDU", EnvironmentVariableTarget.User);

        }

        //load text goals
        private void loadGoals()
        {
            richTextBoxGoals.AppendText("1. Send XML Message\n");
            richTextBoxGoals.AppendText("\n");
            richTextBoxGoals.AppendText("2. Wait for response and manage errors\n");
            richTextBoxGoals.AppendText("   invalid XML message\n");
            richTextBoxGoals.AppendText("   invalid CUPPT message\n");
            richTextBoxGoals.AppendText("\n");
            richTextBoxGoals.AppendText("3.  \n");
        }

        private void buttonConnect_Click(object sender, EventArgs e)
        {

            int port = int.Parse(textBoxPort.Text);
            string host = textBoxHost.Text;

            IPAddress[] addresses = Dns.GetHostAddresses(host);



            //if client does not exist
            if (clientSocket == null)
            {

                clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                listenClient = new Thread(new ThreadStart(ReadMsg));

                listenClient.Start();

                serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                listenServer = new Thread(new ThreadStart(ReadMsg));

                listenServer.Start();

                messageID = 0;

            }


            //try to connect/disconnect
            try
            {
                if (!clientSocket.Connected)
                {
                    connect(host, port);

                    generateXML("interfaceLevelsAvailableRequest");

                    buttonInterfaceLevelsAvailableRequest.Enabled = true;

                }
                else
                {
                    //disconnect(host);

                    MessageBox.Show("Please send byeRequest to disconnect from the platform", "Airline Application", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            catch (SocketException ex)
            {
                listViewEventsAdd(ex.Message);
                //MessageBox.Show("Button Connect Click" + ex.Message);
            }
        }

        private void connect(String host, int port)
        {
            clientSocket.Connect(host, port);

            listViewEventsAdd("Connected to " + host);

            buttonConnect.Text = "1 - Network Socket Disconnection";

            //launch Thread and wait for message from server
            //listenClient.Start();
        }


        private void disconnect(String host)
        {
            listViewEventsAdd("Disconnected to " + host);
            buttonConnect.Text = "1 - Network Socket Connection";

            clientSocket.Close();
            listenClient.Abort();

            serverSocket.Close();
            listenServer.Abort();

            clientSocket = null;
            listenClient = null;


            serverSocket = null;
            listenServer = null;

        }

        private void sendXML(string message)
        {
            generateXML(message);
            // send message
            sendMsg(richTextBoxXML.Text);
        }

        //write
        private void sendMsg(string XML)
        {

            messageID++;

            // replace XML vars
            XML = XML.Replace("{MESSAGEID}", messageID.ToString());
            XML = XML.Replace("{XSDVERSION}", XSDVersion);
            XML = XML.Replace("{INTERFACELEVEL}", InterfaceLevel);

            XML = XML.Replace("{AIRLINE}", AirlineCode);
            XML = XML.Replace("{PLATFORMDEFINEDPARAMETER}", "");

            XML = XML.Replace("{APPLICATIONCODE}", ApplicationCode);
            XML = XML.Replace("{APPLICATIONNAME}", ApplicationName);
            XML = XML.Replace("{APPLICATIONVERSION}", ApplicationVersion);
            XML = XML.Replace("{APPLICATIONDATA}", ApplicationData);

            // send header msg
            XML = generateMsgHeader(XML) + XML;

            //encode string
            byte[] msg = System.Text.Encoding.UTF8.GetBytes(XML);

            int DtSent = clientSocket.Send(msg, msg.Length, SocketFlags.None);

            if (DtSent == 0)
            {
                richTextBoxLogAppendText('E', "No message sent");
            }
            else
            {
                listViewEventsAdd("Message sent : " + XML);
                richTextBoxLogAppendText('S', XML);
            }

        }

        private String generateMsgHeader(String XML)
        {
            // send header msg
            String version = "01";
            String convention = "00";
            String size = XML.Length.ToString("X").ToUpper();


            while (size.Length < 6)
                size = "0" + size;

            Console.WriteLine(XML.Length);
            Console.WriteLine(size);

            return version + convention + size;
        }

        //read
        private void ReadMsg()
        {

            try
            {
                // while (true)
                while (true)
                {

                    if (clientSocket.Connected)
                    {
                        //if client get data
                        if (clientSocket.Available > 0)
                        {

                            string messageReceived = null;
                            // string rtfContent = null;

                            while (clientSocket.Available > 0)
                            {

                                try
                                {
                                    //receive them OLD 
                                    byte[] msg = new Byte[clientSocket.Available];
                                    clientSocket.Receive(msg, 0, clientSocket.Available, SocketFlags.None);

                                    messageReceived = System.Text.Encoding.UTF8.GetString(msg).Trim();

                                    if (rtfContent.Contains("</cupps>"))
                                    {
                                        rtfContent = String.Empty;
                                        rtfContent += messageReceived;

                                    }

                                    //else if (!rtfContent.Contains("</cupps>"))
                                    else
                                    {
                                        rtfContent += messageReceived;

                                    }


                                }

                                catch (SocketException E)
                                {
                                    this.Invoke(new delegateString2(listViewEventsAdd), "CheckData read" + E.Message);
                                   // MessageBox.Show("READ CATCH 1" + E.Message);

                                }
                            }

                            try
                            {
                                //Filling the richtextbox with the data received 
                                // when it was all approved

                                if (messageReceived.Contains("</cupps>"))

                                {
                                    this.Invoke(new delegateString2(listViewEventsAdd), "Message received : " + rtfContent);
                                    this.Invoke(new delegateString2(XMLReader), rtfContent);
                                    this.Invoke(new delegateString(richTextBoxLogAppendText), 'R', rtfContent);
                                    MessageBox.Show(rtfContent);

                                }

                            }

                            catch (Exception E)
                            {
                                //MessageBox.Show(E.Message);
                                System.Diagnostics.Debug.WriteLine(E.Message);
                               // MessageBox.Show("READ CATCH 2" + E.Message);
                            }

                        }

                    }

                    //We wait for 10 milliseconds, to avoid the microprocessor runaway

                    Thread.Sleep(10);
                }

            }

            catch (ThreadStateException e)
            {
                //This thread is likely to be arrested at any time 
                //catch the exception so as not to display a message to the user
                Thread.ResetAbort();
                System.Diagnostics.Debug.WriteLine(e.Message);
               // MessageBox.Show("READ CATCH 3" + e.Message);
            }                    
        }
        //function assigned to the delegate
        private void richTextBoxLogAppendText(char T, string str)
        {
            string symbol = (T == 'S') ? "-->" : "<--";


            richTextBoxLog.AppendText(T + " " + symbol + " " + DateTime.Now.ToString("dd/mm/yyyy HH:mm:ss.fff") + " :\n" + str + "\n\r");
            richTextBoxLog.ScrollToCaret();
        }

        //function assigned to the delegate
        private void richTextBoxXMLRequestAppendText(string str)
        {
            //richTextBoxXMLRequest.Text = str;
        }


        //add Events in ListView
        private void listViewEventsAdd(string str)
        {
            ListViewItem lvItem = new ListViewItem(DateTime.Now.ToString("dd/mm/yyyy HH:mm:ss.fff"));
            lvItem.SubItems.Add(str);

            listViewEvents.Items.Add(lvItem);

            Console.WriteLine(str);
        }

        /*
         other events
         */

        //clear messages whend doubleclick
        private void richTextBoxLog_DoubleClick(object sender, EventArgs e)
        {
            //richTextBoxLog.Clear();
        }

        //before closing, checks whether the socket is not connected
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {

            if (clientSocket != null && clientSocket.Connected)
            {
                //MessageBox.Show("You are connected !", "Airline Application");
                //e.Cancel = true;
                disconnect(textBoxHost.Text);
            }
            else
            {
                /*
                // Display a MsgBox asking the user to save changes or abort.
                if (MessageBox.Show("Are you sure you want to quit ?", "Airline Application", MessageBoxButtons.YesNo) == DialogResult.No)
                {
                    // Cancel the Closing event from closing the form.
                    e.Cancel = true;

                }
                */
            }
        }

        private void buttonInterfaceLevelRequest_Click(object sender, EventArgs e)
        {
            if (listViewInterfaceLevels.SelectedItems.Count > 0)
            {
                InterfaceLevel = listViewInterfaceLevels.SelectedItems[0].SubItems[0].Text;
                XSDLocalPath = listViewInterfaceLevels.SelectedItems[0].SubItems[1].Text;
                XSDVersion = listViewInterfaceLevels.SelectedItems[0].SubItems[2].Text;

                //send message
                sendXML("interfaceLevelRequest");

            }
            else
            {
                MessageBox.Show("Please select Interface Level", "Airline Application", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

            }
        }

        private void buttonObsoleteInterfaceLeveRequest_Click(object sender, EventArgs e)
        {
            InterfaceLevel = "00.00";
            XSDLocalPath = "00.00";
            XSDVersion = "00.00";

            //send message
            sendXML("interfaceLevelRequest");
        }

        private void buttonUnsupportedInterfaceLevelRequest_Click(object sender, EventArgs e)
        {
            InterfaceLevel = "01.02";
            XSDLocalPath = "01.02";
            XSDVersion = "01.02";

            // send message		
            sendXML("interfaceLevelRequest");
        }

        private void buttonAuthenticateRequest_Click(object sender, EventArgs e)
        {
            if (textBoxDeviceToken.Text.Equals(""))
            {
                listViewDeviceList.Items.Clear();

                // send message
                sendXML("authenticateRequest");
            }
            else
            {
                MessageBox.Show("Already authenticated", "Airline Application", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void buttonInterfaceLevelsAvailableRequest_Click(object sender, EventArgs e)
        {

            //send message
            sendXML("interfaceLevelsAvailableRequest");
        }

        /*
          XML
         */

        private void XMLReader(String messageXML)
        {
          
            try
            {
                if (messageXML.IndexOf("<") != -1)
                {
                    try
                    {
                        
                    String xmlString = messageXML.Substring(messageXML.IndexOf("<"));

                    // Create an XmlReader

                    XmlReader reader = XmlReader.Create(new StringReader(xmlString));

                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(xmlString);
                    XmlDocument document = new XmlDocument();

                    if (doc.DocumentElement.NamespaceURI.Length > 0)
                    {
                        doc.DocumentElement.SetAttribute("xmlns", "");
                        // must serialize and reload for this to take effect

                        document.LoadXml(doc.OuterXml);

                    }

                    else

                    {
                        document = doc;

                    }


                    XmlNode node = document.GetElementsByTagName("cupps").Item(0);
                    string messageName = node.Attributes.GetNamedItem("messageName").Value;
             

                    StringBuilder output = new StringBuilder();

                     richTextBoxXMLValidate.Clear();

                        if (messageName.Equals("interfaceLevelsAvailableResponse"))
                        {

                            //change tabPage
                            tabControlLessons.SelectedTab = tabControlLessons.TabPages[1];

                            XmlNodeList nodeList = document.GetElementsByTagName("interfaceLevel");
                            for (int i = 0; i < nodeList.Count; i++)
                            {
                                node = nodeList[i];

                                if (node.Attributes.Count > 0)
                                {
                                    XmlAttributeCollection attrs = node.Attributes;

                                    ListViewItem lvItem = new ListViewItem(attrs.GetNamedItem("level").Value);

                                    lvItem.SubItems.Add(attrs.GetNamedItem("wsLocalPath").Value);
                                    lvItem.SubItems.Add(attrs.GetNamedItem("xsdVersion").Value);

                                    listViewInterfaceLevels.Items.Add(lvItem);

                                }
                            }
                        }
                        else if (messageName.Equals("interfaceLevelResponse"))
                        {
                            node = document.GetElementsByTagName("interfaceLevelResponse").Item(0);

                            if (node.Attributes.GetNamedItem("result").Value.Equals("OK"))
                            {
                                //change tabPage
                                tabControlLessons.SelectedTab = tabControlLessons.TabPages[2];

                            }

                        }
                        else if (messageName.Equals("authenticateResponse"))
                        {
                           

                            node = document.GetElementsByTagName("authenticateResponse").Item(0);

                            textBoxDeviceToken.Text = node.Attributes.GetNamedItem("deviceToken").Value;

                            XmlNodeList nodeList = document.GetElementsByTagName("device");

                            if (InterfaceLevel.Trim().StartsWith("00."))
                             {

                                 Console.WriteLine("CUPPT Interface"); // CUPPT
                                                                       // interface

                                 if (node.Attributes.GetNamedItem("result").Value.Equals("OK"))
                                 { 
                                     for (int i = 0; i < nodeList.Count; i++)
                                 {
                                     node = nodeList[i];

                                     XmlAttributeCollection attrs = node.Attributes;
                                     ListViewItem lvItem = new ListViewItem(attrs.GetNamedItem("deviceName").Value);
                                     ListViewItem acqItem = new ListViewItem(attrs.GetNamedItem("deviceName").Value);

                                     lvItem.SubItems.Add(attrs.GetNamedItem("hostName").Value);
                                     lvItem.SubItems.Add(attrs.GetNamedItem("IP").Value);
                                     lvItem.SubItems.Add(attrs.GetNamedItem("Port").Value);
                                     //lvItem.SubItems.Add(attrs.GetNamedItem("Vendor").Value);
                                     //lvItem.SubItems.Add(attrs.GetNamedItem("Model").Value);
                                     lvItem.SubItems.Add(attrs.GetNamedItem("Status").Value);
                                     listViewDeviceList.Items.Add(lvItem);

                                     
                                     acqItem.SubItems.Add(attrs.GetNamedItem("IP").Value);
                                     acqItem.SubItems.Add(attrs.GetNamedItem("Port").Value);
                                     listViewDeviceAcquireList.Items.Add(acqItem);
                                 }

                             }
                             }

                            else 
                            {




                            for (int i = 0; i < nodeList.Count; i++)
                                {
                                    node = nodeList[i];
                                    XmlAttributeCollection attrs = node.Attributes;
                                    ListViewItem lvItem = new ListViewItem(attrs.GetNamedItem("deviceName").Value);
                                    ListViewItem acqItem = new ListViewItem(attrs.GetNamedItem("deviceName").Value);

                                    if (node.HasChildNodes)
                                     {
                                        XmlNodeList children = node.ChildNodes;
                                        for (int j = 0; j < children.Count; j++)
                                        {
                                            XmlNode child = children.Item(j);

                                            if (child.Name.Contains("DeviceParameter"))
                                            {
                                                XmlNodeList subchildren = child.ChildNodes;

                                                for (int k = 0; k < subchildren.Count; k++)
                                                {                                      
                                                    XmlNode subChild = subchildren.Item(k);

                                                    if (subChild.Name == "ipAndPort")
                                                    {
                                                        XmlAttributeCollection attrs1 = subChild.Attributes;

                                                        lvItem.SubItems.Add(attrs1.GetNamedItem("hostName").Value);
                                                        lvItem.SubItems.Add(attrs1.GetNamedItem("ip").Value);
                                                        lvItem.SubItems.Add(attrs1.GetNamedItem("port").Value);

                                                        acqItem.SubItems.Add(attrs1.GetNamedItem("ip").Value);
                                                        acqItem.SubItems.Add(attrs1.GetNamedItem("port").Value);
                                                        // listViewDeviceList.Items.Insert(2,lvItem);

                                                    }


                                                 /*  else if (subChild.Name == "vendorModelInfo")
                                                     {
                                                         XmlAttributeCollection attrs1 = subChild.Attributes;

                                                         lvItem.SubItems.Add(attrs1.GetNamedItem("vendor").Value);
                                                         lvItem.SubItems.Add(attrs1.GetNamedItem("model").Value);

                                                         // listViewDeviceList.Items.Insert(2,lvItem);

                                                     }*/

                                                    else if (subChild.Name.Contains("Status"))
                                                    {
                                                        XmlAttributeCollection attrs1 = subChild.Attributes;
                                                        int status_true = 0;
                                                        for (int l = 0; l < attrs1.Count; l++)
                                                        {
                                                            //MessageBox.Show("inside l loop");
                                                            if (attrs1.Item(l).Value.Equals("true"))
                                                            {
                                                                // MessageBox.Show("inside status");
                                                                status_true = status_true + 1;
                                                                lvItem.SubItems.Add(attrs1.Item(l).Name);
                                                            }
                                                        }

                                                        // if (lvItem.SubItems.Equals(null))
                                                        //  { lvItem.SubItems.Add("offline"); }

                                                        if (status_true < 1)
                                                        { lvItem.SubItems.Add("Offline"); }

                                                    }

                                                }
                                            }
                                        }

                                        listViewDeviceList.Items.Add(lvItem);
                                        listViewDeviceAcquireList.Items.Add(acqItem);
                                        //colorizeDeviceStatus();
                                    }

                                }                                                      
                          
                                        }
                                   }                  
                    }

                    catch (XmlException e)
            {
                //Console.WriteLine(e.Message);
                System.Diagnostics.Debug.WriteLine(e.Message);
            }
        }
    }
            catch (Exception e)
            {
                //Console.WriteLine(e.Message);
              //  MessageBox.Show(e.Message);
                System.Diagnostics.Debug.WriteLine(e.Message);
            }
            }
        
        //increase messageID in XML Message
        private void generateXML(String type)
        {
            //header
            String XML = "<cupps xmlns=\"http://www.cupps.aero/cupps/01.03\" messageID=\"{MESSAGEID}\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" messageName = \""
                    + type + "\">";

            if (type == "interfaceLevelsAvailableRequest")
            {
                XML += "<interfaceLevelsAvailableRequest hsXSDVersion=\"{XSDVersion}\"/>";
            }
            else if (type == "interfaceLevelRequest")
            {
                XML += "<interfaceLevelRequest level=\"{INTERFACELEVEL}\"/>";
            }
            else if (type == "authenticateRequest")
            {
                XML += "<authenticateRequest airline=\"{AIRLINE}\" eventToken=\"{APPLICATIONCODE}\" platformDefinedParameter=\"{PLATFORMDEFINEDPARAMETER}\" >";

                XML += "<applicationList>";
                XML += "<application applicationName=\"{APPLICATIONNAME}\" applicationVersion=\"{APPLICATIONVERSION}\" applicationData=\"{APPLICATIONDATA}\"/>";
                XML += "</applicationList>";

                XML += "</authenticateRequest>";
            }
            else if (type == "deviceQueryRequest")
            {
                XML += "<deviceQueryRequest deviceName=\"{DEVICENAME}\"/>";
            }
            else if (type == "deviceAcquireRequest")
            {
                XML += "<deviceAcquireRequest deviceName=\"{DEVICENAME}\" deviceToken=\"{DEVICETOKEN}\" airlineID=\"{AIRLINE}\"/>";

                /*if (!textHostName.getText().equals("") && !textHostIP.getText().equals("")
                        && !textHostPort.getText().equals(""))
                    XML += "<relayTo hostName=\"{HOSTNAME}\" ip=\"{HOSTIP}\" port=\"{HOSTPORT}\"/>";*/
            }
            else if (type == "interfaceModeRequest")
            {

                XML += "<interfaceModeRequest mode=\"{INTERFACEMODE}\"></interfaceModeRequest>";

            }
            else if (type == "deviceLockRequest")
            {

                XML += "<deviceLockRequest renew=\"true\"></deviceLockRequest>";

            }
            else if (type == "deviceUnlockRequest")
            {

                XML += "<deviceUnlockRequest></deviceUnlockRequest>";

            }
            else if (type == "aeaRequest")
            {

                XML += "<aeaRequest>";
                XML += "<aeaText><![CDATA[{AEATEXT}]]></aeaText>";

                /*if (AeaRequestType.equals("pcx"))
                {
                    XML += "<aeaBinary><![CDATA[{AEABINARY}]]></aeaBinary>";
                }*/

                XML += "</aeaRequest>";

            }
            else if (type == "deviceReleaseRequest")
            {

                XML += "<deviceReleaseRequest></deviceReleaseRequest>";

            }
            else if (type == "readerReadRequest")
            {

                XML += "<readerReadRequest></readerReadRequest>";

            }

            // end
            XML += "</cupps>";

            //richTextBoxXML.Text = XML;
            richTextBoxXML.Text = XML;
        }

        private void buttonUnsupportedInterfaceLevelRequest_Click_1(object sender, EventArgs e)
        {

        }

        private void buttonObsoleteInterfaceLeveRequest_Click_1(object sender, EventArgs e)
        {

        }

        private void listViewDeviceList_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }

}
