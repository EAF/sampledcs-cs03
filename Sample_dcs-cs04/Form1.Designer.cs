﻿

namespace Sample_dcs_cs03
{
    partial class Form1
    {

        private System.ComponentModel.IContainer components = null;


        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form


        private void InitializeComponent()
        {
            this.groupBoxSocketConnection = new System.Windows.Forms.GroupBox();
            this.textBoxXSDU = new System.Windows.Forms.TextBox();
            this.textBoxACN = new System.Windows.Forms.TextBox();
            this.textBoxCN = new System.Windows.Forms.TextBox();
            this.labelACN = new System.Windows.Forms.Label();
            this.labelCN = new System.Windows.Forms.Label();
            this.textBoxUN = new System.Windows.Forms.TextBox();
            this.textBoxOPR = new System.Windows.Forms.TextBox();
            this.textBoxPLT = new System.Windows.Forms.TextBox();
            this.labelXSDU = new System.Windows.Forms.Label();
            this.textBoxPort = new System.Windows.Forms.TextBox();
            this.labelUN = new System.Windows.Forms.Label();
            this.textBoxHost = new System.Windows.Forms.TextBox();
            this.labelOPR = new System.Windows.Forms.Label();
            this.labePLT = new System.Windows.Forms.Label();
            this.labelPort = new System.Windows.Forms.Label();
            this.labelHost = new System.Windows.Forms.Label();
            this.buttonConnect = new System.Windows.Forms.Button();
            this.richTextBoxLog = new System.Windows.Forms.RichTextBox();
            this.buttonInterfaceLevelsAvailableRequest = new System.Windows.Forms.Button();
            this.groupBoxGoals = new System.Windows.Forms.GroupBox();
            this.richTextBoxGoals = new System.Windows.Forms.RichTextBox();
            this.richTextBoxXML = new System.Windows.Forms.RichTextBox();
            this.tabControlLessons = new System.Windows.Forms.TabControl();
            this.tabPageLesson1 = new System.Windows.Forms.TabPage();
            this.labelXML = new System.Windows.Forms.Label();
            this.tabPageLesson2 = new System.Windows.Forms.TabPage();
            this.buttonUnsupportedInterfaceLevelRequest = new System.Windows.Forms.Button();
            this.buttonObsoleteInterfaceLeveRequest = new System.Windows.Forms.Button();
            this.buttonInterfaceLevelRequest = new System.Windows.Forms.Button();
            this.listViewInterfaceLevels = new System.Windows.Forms.ListView();
            this.columnHeaderInterfaceLevel = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderInterfaceLocalPath = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderInterfaceXSDVersion = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label3 = new System.Windows.Forms.Label();
            this.richTextBoxXMLValidate = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.buttonAuthenticateRequest = new System.Windows.Forms.Button();
            this.labelDeviceToken = new System.Windows.Forms.Label();
            this.textBoxDeviceToken = new System.Windows.Forms.TextBox();
            this.listViewDeviceList = new System.Windows.Forms.ListView();
            this.columnHeaderDeviceName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderHostName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderIP = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderPort = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderStatus = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.labelDeviceList = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label7 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lblDiskError = new System.Windows.Forms.Label();
            this.lbOrangePaperjam = new System.Windows.Forms.Label();
            this.lblGreenReady = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textHostPort = new System.Windows.Forms.TextBox();
            this.textHostIP = new System.Windows.Forms.TextBox();
            this.textHostName = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.listViewConnectedDeviceList = new System.Windows.Forms.ListView();
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnDeviceAcquireRequest = new System.Windows.Forms.Button();
            this.listViewDeviceAcquireList = new System.Windows.Forms.ListView();
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label2 = new System.Windows.Forms.Label();
            this.listViewEvents = new System.Windows.Forms.ListView();
            this.columnTime = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnEvent = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupBoxSocketConnection.SuspendLayout();
            this.groupBoxGoals.SuspendLayout();
            this.tabControlLessons.SuspendLayout();
            this.tabPageLesson1.SuspendLayout();
            this.tabPageLesson2.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxSocketConnection
            // 
            this.groupBoxSocketConnection.Controls.Add(this.textBoxXSDU);
            this.groupBoxSocketConnection.Controls.Add(this.textBoxACN);
            this.groupBoxSocketConnection.Controls.Add(this.textBoxCN);
            this.groupBoxSocketConnection.Controls.Add(this.labelACN);
            this.groupBoxSocketConnection.Controls.Add(this.labelCN);
            this.groupBoxSocketConnection.Controls.Add(this.textBoxUN);
            this.groupBoxSocketConnection.Controls.Add(this.textBoxOPR);
            this.groupBoxSocketConnection.Controls.Add(this.textBoxPLT);
            this.groupBoxSocketConnection.Controls.Add(this.labelXSDU);
            this.groupBoxSocketConnection.Controls.Add(this.textBoxPort);
            this.groupBoxSocketConnection.Controls.Add(this.labelUN);
            this.groupBoxSocketConnection.Controls.Add(this.textBoxHost);
            this.groupBoxSocketConnection.Controls.Add(this.labelOPR);
            this.groupBoxSocketConnection.Controls.Add(this.labePLT);
            this.groupBoxSocketConnection.Controls.Add(this.labelPort);
            this.groupBoxSocketConnection.Controls.Add(this.labelHost);
            this.groupBoxSocketConnection.Location = new System.Drawing.Point(12, 12);
            this.groupBoxSocketConnection.Name = "groupBoxSocketConnection";
            this.groupBoxSocketConnection.Size = new System.Drawing.Size(931, 105);
            this.groupBoxSocketConnection.TabIndex = 0;
            this.groupBoxSocketConnection.TabStop = false;
            this.groupBoxSocketConnection.Text = "Platform Connection";
            // 
            // textBoxXSDU
            // 
            this.textBoxXSDU.Location = new System.Drawing.Point(302, 79);
            this.textBoxXSDU.Name = "textBoxXSDU";
            this.textBoxXSDU.Size = new System.Drawing.Size(312, 20);
            this.textBoxXSDU.TabIndex = 13;
            // 
            // textBoxACN
            // 
            this.textBoxACN.Location = new System.Drawing.Point(804, 47);
            this.textBoxACN.Name = "textBoxACN";
            this.textBoxACN.Size = new System.Drawing.Size(121, 20);
            this.textBoxACN.TabIndex = 12;
            // 
            // textBoxCN
            // 
            this.textBoxCN.Location = new System.Drawing.Point(804, 17);
            this.textBoxCN.Name = "textBoxCN";
            this.textBoxCN.Size = new System.Drawing.Size(121, 20);
            this.textBoxCN.TabIndex = 11;
            // 
            // labelACN
            // 
            this.labelACN.AutoSize = true;
            this.labelACN.Location = new System.Drawing.Point(651, 50);
            this.labelACN.Name = "labelACN";
            this.labelACN.Size = new System.Drawing.Size(147, 13);
            this.labelACN.TabIndex = 10;
            this.labelACN.Text = "Alternate Name (CUPPSACN)";
            // 
            // labelCN
            // 
            this.labelCN.AutoSize = true;
            this.labelCN.Location = new System.Drawing.Point(651, 20);
            this.labelCN.Name = "labelCN";
            this.labelCN.Size = new System.Drawing.Size(143, 13);
            this.labelCN.TabIndex = 9;
            this.labelCN.Text = "Computer Name (CUPPSCN)";
            // 
            // textBoxUN
            // 
            this.textBoxUN.Location = new System.Drawing.Point(534, 47);
            this.textBoxUN.Name = "textBoxUN";
            this.textBoxUN.Size = new System.Drawing.Size(80, 20);
            this.textBoxUN.TabIndex = 8;
            // 
            // textBoxOPR
            // 
            this.textBoxOPR.Location = new System.Drawing.Point(534, 17);
            this.textBoxOPR.Name = "textBoxOPR";
            this.textBoxOPR.Size = new System.Drawing.Size(80, 20);
            this.textBoxOPR.TabIndex = 7;
            // 
            // textBoxPLT
            // 
            this.textBoxPLT.Location = new System.Drawing.Point(115, 17);
            this.textBoxPLT.Name = "textBoxPLT";
            this.textBoxPLT.Size = new System.Drawing.Size(80, 20);
            this.textBoxPLT.TabIndex = 6;
            // 
            // labelXSDU
            // 
            this.labelXSDU.AutoSize = true;
            this.labelXSDU.Location = new System.Drawing.Point(207, 82);
            this.labelXSDU.Name = "labelXSDU";
            this.labelXSDU.Size = new System.Drawing.Size(71, 13);
            this.labelXSDU.TabIndex = 5;
            this.labelXSDU.Text = "XSD Schema";
            // 
            // textBoxPort
            // 
            this.textBoxPort.Location = new System.Drawing.Point(302, 47);
            this.textBoxPort.Name = "textBoxPort";
            this.textBoxPort.Size = new System.Drawing.Size(80, 20);
            this.textBoxPort.TabIndex = 4;
            // 
            // labelUN
            // 
            this.labelUN.AutoSize = true;
            this.labelUN.Location = new System.Drawing.Point(410, 50);
            this.labelUN.Name = "labelUN";
            this.labelUN.Size = new System.Drawing.Size(118, 13);
            this.labelUN.TabIndex = 2;
            this.labelUN.Text = "UserName (CUPPSUN)";
            // 
            // textBoxHost
            // 
            this.textBoxHost.Location = new System.Drawing.Point(302, 17);
            this.textBoxHost.Name = "textBoxHost";
            this.textBoxHost.Size = new System.Drawing.Size(80, 20);
            this.textBoxHost.TabIndex = 3;
            // 
            // labelOPR
            // 
            this.labelOPR.AutoSize = true;
            this.labelOPR.Location = new System.Drawing.Point(410, 20);
            this.labelOPR.Name = "labelOPR";
            this.labelOPR.Size = new System.Drawing.Size(116, 13);
            this.labelOPR.TabIndex = 1;
            this.labelOPR.Text = "Operator (CUPPSOPR)";
            // 
            // labePLT
            // 
            this.labePLT.AutoSize = true;
            this.labePLT.Location = new System.Drawing.Point(10, 20);
            this.labePLT.Name = "labePLT";
            this.labePLT.Size = new System.Drawing.Size(99, 13);
            this.labePLT.TabIndex = 0;
            this.labePLT.Text = "Site (CUPPSPLT) : ";
            // 
            // labelPort
            // 
            this.labelPort.AutoSize = true;
            this.labelPort.Location = new System.Drawing.Point(203, 50);
            this.labelPort.Name = "labelPort";
            this.labelPort.Size = new System.Drawing.Size(85, 13);
            this.labelPort.TabIndex = 1;
            this.labelPort.Text = "Port (CUPPSPP)";
            // 
            // labelHost
            // 
            this.labelHost.AutoSize = true;
            this.labelHost.Location = new System.Drawing.Point(203, 20);
            this.labelHost.Name = "labelHost";
            this.labelHost.Size = new System.Drawing.Size(93, 13);
            this.labelHost.TabIndex = 0;
            this.labelHost.Text = "Node (CUPPSPN)";
            // 
            // buttonConnect
            // 
            this.buttonConnect.Location = new System.Drawing.Point(452, 38);
            this.buttonConnect.Name = "buttonConnect";
            this.buttonConnect.Size = new System.Drawing.Size(192, 23);
            this.buttonConnect.TabIndex = 2;
            this.buttonConnect.Text = "1 - Network Socket Connection";
            this.buttonConnect.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonConnect.UseVisualStyleBackColor = true;
            this.buttonConnect.Click += new System.EventHandler(this.buttonConnect_Click);
            // 
            // richTextBoxLog
            // 
            this.richTextBoxLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBoxLog.Location = new System.Drawing.Point(6, 140);
            this.richTextBoxLog.Name = "richTextBoxLog";
            this.richTextBoxLog.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.richTextBoxLog.Size = new System.Drawing.Size(683, 164);
            this.richTextBoxLog.TabIndex = 7;
            this.richTextBoxLog.Text = "";
            // 
            // buttonInterfaceLevelsAvailableRequest
            // 
            this.buttonInterfaceLevelsAvailableRequest.Enabled = false;
            this.buttonInterfaceLevelsAvailableRequest.Location = new System.Drawing.Point(452, 67);
            this.buttonInterfaceLevelsAvailableRequest.Name = "buttonInterfaceLevelsAvailableRequest";
            this.buttonInterfaceLevelsAvailableRequest.Size = new System.Drawing.Size(192, 23);
            this.buttonInterfaceLevelsAvailableRequest.TabIndex = 9;
            this.buttonInterfaceLevelsAvailableRequest.Text = "2 - InterfaceLevelsAvailableRequest";
            this.buttonInterfaceLevelsAvailableRequest.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonInterfaceLevelsAvailableRequest.UseVisualStyleBackColor = true;
            this.buttonInterfaceLevelsAvailableRequest.Click += new System.EventHandler(this.buttonInterfaceLevelsAvailableRequest_Click);
            // 
            // groupBoxGoals
            // 
            this.groupBoxGoals.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxGoals.Controls.Add(this.richTextBoxGoals);
            this.groupBoxGoals.Location = new System.Drawing.Point(721, 137);
            this.groupBoxGoals.Name = "groupBoxGoals";
            this.groupBoxGoals.Size = new System.Drawing.Size(267, 462);
            this.groupBoxGoals.TabIndex = 11;
            this.groupBoxGoals.TabStop = false;
            this.groupBoxGoals.Text = "Lesson Goals";
            // 
            // richTextBoxGoals
            // 
            this.richTextBoxGoals.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBoxGoals.BackColor = System.Drawing.Color.Khaki;
            this.richTextBoxGoals.Location = new System.Drawing.Point(8, 19);
            this.richTextBoxGoals.Name = "richTextBoxGoals";
            this.richTextBoxGoals.Size = new System.Drawing.Size(255, 437);
            this.richTextBoxGoals.TabIndex = 0;
            this.richTextBoxGoals.Text = "";
            // 
            // richTextBoxXML
            // 
            this.richTextBoxXML.Location = new System.Drawing.Point(6, 38);
            this.richTextBoxXML.Name = "richTextBoxXML";
            this.richTextBoxXML.Size = new System.Drawing.Size(440, 96);
            this.richTextBoxXML.TabIndex = 12;
            this.richTextBoxXML.Text = "";
            // 
            // tabControlLessons
            // 
            this.tabControlLessons.Controls.Add(this.tabPageLesson1);
            this.tabControlLessons.Controls.Add(this.tabPageLesson2);
            this.tabControlLessons.Controls.Add(this.tabPage1);
            this.tabControlLessons.Controls.Add(this.tabPage2);
            this.tabControlLessons.Location = new System.Drawing.Point(12, 123);
            this.tabControlLessons.Name = "tabControlLessons";
            this.tabControlLessons.SelectedIndex = 0;
            this.tabControlLessons.Size = new System.Drawing.Size(703, 336);
            this.tabControlLessons.TabIndex = 13;
            // 
            // tabPageLesson1
            // 
            this.tabPageLesson1.Controls.Add(this.labelXML);
            this.tabPageLesson1.Controls.Add(this.richTextBoxXML);
            this.tabPageLesson1.Controls.Add(this.richTextBoxLog);
            this.tabPageLesson1.Controls.Add(this.buttonConnect);
            this.tabPageLesson1.Controls.Add(this.buttonInterfaceLevelsAvailableRequest);
            this.tabPageLesson1.Location = new System.Drawing.Point(4, 22);
            this.tabPageLesson1.Name = "tabPageLesson1";
            this.tabPageLesson1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageLesson1.Size = new System.Drawing.Size(695, 310);
            this.tabPageLesson1.TabIndex = 0;
            this.tabPageLesson1.Text = "Lesson 1";
            this.tabPageLesson1.UseVisualStyleBackColor = true;
            // 
            // labelXML
            // 
            this.labelXML.AutoSize = true;
            this.labelXML.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.labelXML.Location = new System.Drawing.Point(126, 22);
            this.labelXML.Name = "labelXML";
            this.labelXML.Size = new System.Drawing.Size(239, 13);
            this.labelXML.TabIndex = 13;
            this.labelXML.Text = "XML Message : interfaceLevelsAvailableRequest";
            // 
            // tabPageLesson2
            // 
            this.tabPageLesson2.Controls.Add(this.buttonUnsupportedInterfaceLevelRequest);
            this.tabPageLesson2.Controls.Add(this.buttonObsoleteInterfaceLeveRequest);
            this.tabPageLesson2.Controls.Add(this.buttonInterfaceLevelRequest);
            this.tabPageLesson2.Controls.Add(this.listViewInterfaceLevels);
            this.tabPageLesson2.Controls.Add(this.label3);
            this.tabPageLesson2.Controls.Add(this.richTextBoxXMLValidate);
            this.tabPageLesson2.Controls.Add(this.label1);
            this.tabPageLesson2.Location = new System.Drawing.Point(4, 22);
            this.tabPageLesson2.Name = "tabPageLesson2";
            this.tabPageLesson2.Size = new System.Drawing.Size(695, 310);
            this.tabPageLesson2.TabIndex = 1;
            this.tabPageLesson2.Text = "Lesson 2";
            this.tabPageLesson2.UseVisualStyleBackColor = true;
            // 
            // buttonUnsupportedInterfaceLevelRequest
            // 
            this.buttonUnsupportedInterfaceLevelRequest.ForeColor = System.Drawing.Color.Red;
            this.buttonUnsupportedInterfaceLevelRequest.Location = new System.Drawing.Point(452, 105);
            this.buttonUnsupportedInterfaceLevelRequest.Name = "buttonUnsupportedInterfaceLevelRequest";
            this.buttonUnsupportedInterfaceLevelRequest.Size = new System.Drawing.Size(192, 23);
            this.buttonUnsupportedInterfaceLevelRequest.TabIndex = 24;
            this.buttonUnsupportedInterfaceLevelRequest.Text = "Unsupported interfaceLevelRequest";
            this.buttonUnsupportedInterfaceLevelRequest.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonUnsupportedInterfaceLevelRequest.UseVisualStyleBackColor = true;
            // 
            // buttonObsoleteInterfaceLeveRequest
            // 
            this.buttonObsoleteInterfaceLeveRequest.ForeColor = System.Drawing.Color.Red;
            this.buttonObsoleteInterfaceLeveRequest.Location = new System.Drawing.Point(452, 134);
            this.buttonObsoleteInterfaceLeveRequest.Name = "buttonObsoleteInterfaceLeveRequest";
            this.buttonObsoleteInterfaceLeveRequest.Size = new System.Drawing.Size(192, 23);
            this.buttonObsoleteInterfaceLeveRequest.TabIndex = 23;
            this.buttonObsoleteInterfaceLeveRequest.Text = "Obsolete interfaceLevelRequest";
            this.buttonObsoleteInterfaceLeveRequest.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonObsoleteInterfaceLeveRequest.UseVisualStyleBackColor = true;
            // 
            // buttonInterfaceLevelRequest
            // 
            this.buttonInterfaceLevelRequest.Location = new System.Drawing.Point(452, 27);
            this.buttonInterfaceLevelRequest.Name = "buttonInterfaceLevelRequest";
            this.buttonInterfaceLevelRequest.Size = new System.Drawing.Size(192, 23);
            this.buttonInterfaceLevelRequest.TabIndex = 22;
            this.buttonInterfaceLevelRequest.Text = "3 - interfaceLevelRequest";
            this.buttonInterfaceLevelRequest.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonInterfaceLevelRequest.UseVisualStyleBackColor = true;
            this.buttonInterfaceLevelRequest.Click += new System.EventHandler(this.buttonInterfaceLevelRequest_Click);
            // 
            // listViewInterfaceLevels
            // 
            this.listViewInterfaceLevels.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeaderInterfaceLevel,
            this.columnHeaderInterfaceLocalPath,
            this.columnHeaderInterfaceXSDVersion});
            this.listViewInterfaceLevels.FullRowSelect = true;
            this.listViewInterfaceLevels.GridLines = true;
            this.listViewInterfaceLevels.HideSelection = false;
            this.listViewInterfaceLevels.Location = new System.Drawing.Point(9, 27);
            this.listViewInterfaceLevels.MultiSelect = false;
            this.listViewInterfaceLevels.Name = "listViewInterfaceLevels";
            this.listViewInterfaceLevels.Size = new System.Drawing.Size(440, 130);
            this.listViewInterfaceLevels.TabIndex = 15;
            this.listViewInterfaceLevels.UseCompatibleStateImageBehavior = false;
            this.listViewInterfaceLevels.View = System.Windows.Forms.View.Details;
            // 
            // columnHeaderInterfaceLevel
            // 
            this.columnHeaderInterfaceLevel.Tag = "";
            this.columnHeaderInterfaceLevel.Text = "Level";
            // 
            // columnHeaderInterfaceLocalPath
            // 
            this.columnHeaderInterfaceLocalPath.Text = "Local Path";
            this.columnHeaderInterfaceLocalPath.Width = 260;
            // 
            // columnHeaderInterfaceXSDVersion
            // 
            this.columnHeaderInterfaceXSDVersion.Text = "XSD Version";
            this.columnHeaderInterfaceXSDVersion.Width = 100;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label3.Location = new System.Drawing.Point(3, 160);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(175, 13);
            this.label3.TabIndex = 21;
            this.label3.Text = "XML Message Response Validation";
            // 
            // richTextBoxXMLValidate
            // 
            this.richTextBoxXMLValidate.Location = new System.Drawing.Point(6, 176);
            this.richTextBoxXMLValidate.Name = "richTextBoxXMLValidate";
            this.richTextBoxXMLValidate.Size = new System.Drawing.Size(638, 128);
            this.richTextBoxXMLValidate.TabIndex = 20;
            this.richTextBoxXMLValidate.Text = "";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label1.Location = new System.Drawing.Point(3, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(148, 13);
            this.label1.TabIndex = 18;
            this.label1.Text = "Interface Levels Available List";
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.buttonAuthenticateRequest);
            this.tabPage1.Controls.Add(this.labelDeviceToken);
            this.tabPage1.Controls.Add(this.textBoxDeviceToken);
            this.tabPage1.Controls.Add(this.listViewDeviceList);
            this.tabPage1.Controls.Add(this.labelDeviceList);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(695, 310);
            this.tabPage1.TabIndex = 2;
            this.tabPage1.Text = "Lesson 3";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // buttonAuthenticateRequest
            // 
            this.buttonAuthenticateRequest.Location = new System.Drawing.Point(504, 34);
            this.buttonAuthenticateRequest.Name = "buttonAuthenticateRequest";
            this.buttonAuthenticateRequest.Size = new System.Drawing.Size(140, 23);
            this.buttonAuthenticateRequest.TabIndex = 18;
            this.buttonAuthenticateRequest.Text = "4 - Authenticate Request";
            this.buttonAuthenticateRequest.UseVisualStyleBackColor = true;
            this.buttonAuthenticateRequest.Click += new System.EventHandler(this.buttonAuthenticateRequest_Click);
            // 
            // labelDeviceToken
            // 
            this.labelDeviceToken.AutoSize = true;
            this.labelDeviceToken.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDeviceToken.ForeColor = System.Drawing.SystemColors.Highlight;
            this.labelDeviceToken.Location = new System.Drawing.Point(359, 3);
            this.labelDeviceToken.Name = "labelDeviceToken";
            this.labelDeviceToken.Size = new System.Drawing.Size(139, 24);
            this.labelDeviceToken.TabIndex = 17;
            this.labelDeviceToken.Text = "Device Token";
            // 
            // textBoxDeviceToken
            // 
            this.textBoxDeviceToken.Location = new System.Drawing.Point(504, 8);
            this.textBoxDeviceToken.Name = "textBoxDeviceToken";
            this.textBoxDeviceToken.Size = new System.Drawing.Size(140, 20);
            this.textBoxDeviceToken.TabIndex = 16;
            // 
            // listViewDeviceList
            // 
            this.listViewDeviceList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeaderDeviceName,
            this.columnHeaderHostName,
            this.columnHeaderIP,
            this.columnHeaderPort,
            this.columnHeaderStatus});
            this.listViewDeviceList.FullRowSelect = true;
            this.listViewDeviceList.GridLines = true;
            this.listViewDeviceList.Location = new System.Drawing.Point(9, 30);
            this.listViewDeviceList.Name = "listViewDeviceList";
            this.listViewDeviceList.Size = new System.Drawing.Size(489, 274);
            this.listViewDeviceList.TabIndex = 15;
            this.listViewDeviceList.UseCompatibleStateImageBehavior = false;
            this.listViewDeviceList.View = System.Windows.Forms.View.Details;
            this.listViewDeviceList.SelectedIndexChanged += new System.EventHandler(this.listViewDeviceList_SelectedIndexChanged);
            // 
            // columnHeaderDeviceName
            // 
            this.columnHeaderDeviceName.Tag = "";
            this.columnHeaderDeviceName.Text = "DeviceName";
            this.columnHeaderDeviceName.Width = 155;
            // 
            // columnHeaderHostName
            // 
            this.columnHeaderHostName.Text = "HostName";
            this.columnHeaderHostName.Width = 103;
            // 
            // columnHeaderIP
            // 
            this.columnHeaderIP.Text = "IP";
            this.columnHeaderIP.Width = 109;
            // 
            // columnHeaderPort
            // 
            this.columnHeaderPort.Text = "Port";
            this.columnHeaderPort.Width = 61;
            // 
            // columnHeaderStatus
            // 
            this.columnHeaderStatus.Text = "Status";
            this.columnHeaderStatus.Width = 195;
            // 
            // labelDeviceList
            // 
            this.labelDeviceList.AutoSize = true;
            this.labelDeviceList.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.labelDeviceList.Location = new System.Drawing.Point(3, 11);
            this.labelDeviceList.Name = "labelDeviceList";
            this.labelDeviceList.Size = new System.Drawing.Size(57, 13);
            this.labelDeviceList.TabIndex = 0;
            this.labelDeviceList.Text = "DeviceList";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.label7);
            this.tabPage2.Controls.Add(this.label10);
            this.tabPage2.Controls.Add(this.lblDiskError);
            this.tabPage2.Controls.Add(this.lbOrangePaperjam);
            this.tabPage2.Controls.Add(this.lblGreenReady);
            this.tabPage2.Controls.Add(this.groupBox1);
            this.tabPage2.Controls.Add(this.listViewConnectedDeviceList);
            this.tabPage2.Controls.Add(this.btnDeviceAcquireRequest);
            this.tabPage2.Controls.Add(this.listViewDeviceAcquireList);
            this.tabPage2.Controls.Add(this.label2);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(695, 310);
            this.tabPage2.TabIndex = 3;
            this.tabPage2.Text = "Lesson 4";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label7.Location = new System.Drawing.Point(553, 11);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(115, 13);
            this.label7.TabIndex = 23;
            this.label7.Text = "Connected Device List";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.ForeColor = System.Drawing.Color.Red;
            this.label10.Location = new System.Drawing.Point(257, 294);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(84, 13);
            this.label10.TabIndex = 22;
            this.label10.Text = "Offline-PowerOff";
            // 
            // lblDiskError
            // 
            this.lblDiskError.AutoSize = true;
            this.lblDiskError.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.lblDiskError.Location = new System.Drawing.Point(257, 268);
            this.lblDiskError.Name = "lblDiskError";
            this.lblDiskError.Size = new System.Drawing.Size(64, 13);
            this.lblDiskError.TabIndex = 21;
            this.lblDiskError.Text = "diskError-init";
            // 
            // lbOrangePaperjam
            // 
            this.lbOrangePaperjam.AutoSize = true;
            this.lbOrangePaperjam.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.lbOrangePaperjam.Location = new System.Drawing.Point(259, 242);
            this.lbOrangePaperjam.Name = "lbOrangePaperjam";
            this.lbOrangePaperjam.Size = new System.Drawing.Size(97, 13);
            this.lbOrangePaperjam.TabIndex = 20;
            this.lbOrangePaperjam.Text = "Paperout-Paperjam";
            // 
            // lblGreenReady
            // 
            this.lblGreenReady.AutoSize = true;
            this.lblGreenReady.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.lblGreenReady.Location = new System.Drawing.Point(259, 217);
            this.lblGreenReady.Name = "lblGreenReady";
            this.lblGreenReady.Size = new System.Drawing.Size(38, 13);
            this.lblGreenReady.TabIndex = 6;
            this.lblGreenReady.Text = "Ready";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textHostPort);
            this.groupBox1.Controls.Add(this.textHostIP);
            this.groupBox1.Controls.Add(this.textHostName);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Location = new System.Drawing.Point(256, 40);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(152, 174);
            this.groupBox1.TabIndex = 19;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Relay to";
            // 
            // textHostPort
            // 
            this.textHostPort.Location = new System.Drawing.Point(4, 139);
            this.textHostPort.Name = "textHostPort";
            this.textHostPort.Size = new System.Drawing.Size(142, 20);
            this.textHostPort.TabIndex = 5;
            // 
            // textHostIP
            // 
            this.textHostIP.Location = new System.Drawing.Point(4, 92);
            this.textHostIP.Name = "textHostIP";
            this.textHostIP.Size = new System.Drawing.Size(142, 20);
            this.textHostIP.TabIndex = 4;
            // 
            // textHostName
            // 
            this.textHostName.Location = new System.Drawing.Point(4, 42);
            this.textHostName.Name = "textHostName";
            this.textHostName.Size = new System.Drawing.Size(142, 20);
            this.textHostName.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(1, 123);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(26, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "Port";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 76);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(17, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "IP";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 26);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "hostName";
            // 
            // listViewConnectedDeviceList
            // 
            this.listViewConnectedDeviceList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader5,
            this.columnHeader6,
            this.columnHeader7,
            this.columnHeader10});
            this.listViewConnectedDeviceList.FullRowSelect = true;
            this.listViewConnectedDeviceList.GridLines = true;
            this.listViewConnectedDeviceList.Location = new System.Drawing.Point(417, 40);
            this.listViewConnectedDeviceList.Name = "listViewConnectedDeviceList";
            this.listViewConnectedDeviceList.Size = new System.Drawing.Size(275, 274);
            this.listViewConnectedDeviceList.TabIndex = 18;
            this.listViewConnectedDeviceList.UseCompatibleStateImageBehavior = false;
            this.listViewConnectedDeviceList.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "DeviceName";
            this.columnHeader5.Width = 80;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "IP";
            this.columnHeader6.Width = 70;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Port";
            this.columnHeader7.Width = 78;
            // 
            // columnHeader10
            // 
            this.columnHeader10.Text = "Status";
            // 
            // btnDeviceAcquireRequest
            // 
            this.btnDeviceAcquireRequest.Location = new System.Drawing.Point(268, 11);
            this.btnDeviceAcquireRequest.Name = "btnDeviceAcquireRequest";
            this.btnDeviceAcquireRequest.Size = new System.Drawing.Size(140, 23);
            this.btnDeviceAcquireRequest.TabIndex = 17;
            this.btnDeviceAcquireRequest.Text = "6.Device Acq. Request";
            this.btnDeviceAcquireRequest.UseVisualStyleBackColor = true;
            // 
            // listViewDeviceAcquireList
            // 
            this.listViewDeviceAcquireList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4});
            this.listViewDeviceAcquireList.FullRowSelect = true;
            this.listViewDeviceAcquireList.GridLines = true;
            this.listViewDeviceAcquireList.Location = new System.Drawing.Point(9, 33);
            this.listViewDeviceAcquireList.Name = "listViewDeviceAcquireList";
            this.listViewDeviceAcquireList.Size = new System.Drawing.Size(241, 274);
            this.listViewDeviceAcquireList.TabIndex = 16;
            this.listViewDeviceAcquireList.UseCompatibleStateImageBehavior = false;
            this.listViewDeviceAcquireList.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "DeviceName";
            this.columnHeader2.Width = 80;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "IP";
            this.columnHeader3.Width = 83;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Port";
            this.columnHeader4.Width = 109;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label2.Location = new System.Drawing.Point(6, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "DeviceList";
            // 
            // listViewEvents
            // 
            this.listViewEvents.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnTime,
            this.columnEvent});
            this.listViewEvents.FullRowSelect = true;
            this.listViewEvents.GridLines = true;
            this.listViewEvents.Location = new System.Drawing.Point(12, 465);
            this.listViewEvents.Name = "listViewEvents";
            this.listViewEvents.Size = new System.Drawing.Size(703, 134);
            this.listViewEvents.TabIndex = 14;
            this.listViewEvents.UseCompatibleStateImageBehavior = false;
            this.listViewEvents.View = System.Windows.Forms.View.Details;
            // 
            // columnTime
            // 
            this.columnTime.Tag = "";
            this.columnTime.Text = "Time";
            this.columnTime.Width = 200;
            // 
            // columnEvent
            // 
            this.columnEvent.Text = "Event";
            this.columnEvent.Width = 400;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1000, 611);
            this.Controls.Add(this.listViewEvents);
            this.Controls.Add(this.tabControlLessons);
            this.Controls.Add(this.groupBoxGoals);
            this.Controls.Add(this.groupBoxSocketConnection);
            this.Name = "Form1";
            this.Text = "CUPPT SDK - lesson 03 C#";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBoxSocketConnection.ResumeLayout(false);
            this.groupBoxSocketConnection.PerformLayout();
            this.groupBoxGoals.ResumeLayout(false);
            this.tabControlLessons.ResumeLayout(false);
            this.tabPageLesson1.ResumeLayout(false);
            this.tabPageLesson1.PerformLayout();
            this.tabPageLesson2.ResumeLayout(false);
            this.tabPageLesson2.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxSocketConnection;
        private System.Windows.Forms.TextBox textBoxHost;
        private System.Windows.Forms.Button buttonConnect;
        private System.Windows.Forms.Label labelPort;
        private System.Windows.Forms.Label labelHost;
        private System.Windows.Forms.TextBox textBoxPort;
        private System.Windows.Forms.RichTextBox richTextBoxLog;
        private System.Windows.Forms.Label labePLT;
        private System.Windows.Forms.Label labelUN;
        private System.Windows.Forms.Label labelOPR;
        private System.Windows.Forms.Button buttonInterfaceLevelsAvailableRequest;
        private System.Windows.Forms.GroupBox groupBoxGoals;
        private System.Windows.Forms.RichTextBox richTextBoxGoals;
        private System.Windows.Forms.RichTextBox richTextBoxXML;
        private System.Windows.Forms.TabControl tabControlLessons;
        private System.Windows.Forms.TabPage tabPageLesson1;
        private System.Windows.Forms.ListView listViewEvents;
        private System.Windows.Forms.ColumnHeader columnTime;
        private System.Windows.Forms.ColumnHeader columnEvent;
        private System.Windows.Forms.TextBox textBoxPLT;
        private System.Windows.Forms.Label labelXSDU;
        private System.Windows.Forms.TextBox textBoxUN;
        private System.Windows.Forms.TextBox textBoxOPR;
        private System.Windows.Forms.TextBox textBoxXSDU;
        private System.Windows.Forms.TextBox textBoxACN;
        private System.Windows.Forms.TextBox textBoxCN;
        private System.Windows.Forms.Label labelACN;
        private System.Windows.Forms.Label labelCN;
        private System.Windows.Forms.Label labelXML;
        private System.Windows.Forms.TabPage tabPageLesson2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RichTextBox richTextBoxXMLValidate;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListView listViewInterfaceLevels;
        private System.Windows.Forms.ColumnHeader columnHeaderInterfaceLevel;
        private System.Windows.Forms.ColumnHeader columnHeaderInterfaceLocalPath;
        private System.Windows.Forms.ColumnHeader columnHeaderInterfaceXSDVersion;
        private System.Windows.Forms.Button buttonInterfaceLevelRequest;
        private System.Windows.Forms.Button buttonObsoleteInterfaceLeveRequest;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.ListView listViewDeviceList;
        private System.Windows.Forms.ColumnHeader columnHeaderDeviceName;
        private System.Windows.Forms.ColumnHeader columnHeaderHostName;
        private System.Windows.Forms.ColumnHeader columnHeaderIP;
        private System.Windows.Forms.ColumnHeader columnHeaderPort;
        private System.Windows.Forms.ColumnHeader columnHeaderStatus;
        private System.Windows.Forms.Label labelDeviceList;
        private System.Windows.Forms.TextBox textBoxDeviceToken;
        private System.Windows.Forms.Label labelDeviceToken;
        private System.Windows.Forms.Button buttonAuthenticateRequest;
        private System.Windows.Forms.Button buttonUnsupportedInterfaceLevelRequest;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.ListView listViewDeviceAcquireList;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnDeviceAcquireRequest;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ListView listViewConnectedDeviceList;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader10;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textHostPort;
        private System.Windows.Forms.TextBox textHostIP;
        private System.Windows.Forms.TextBox textHostName;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lbOrangePaperjam;
        private System.Windows.Forms.Label lblGreenReady;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lblDiskError;
        private System.Windows.Forms.Label label7;
    }
}






